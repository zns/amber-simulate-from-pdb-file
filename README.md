# Simulate PDB with Amber

This program allows you to do an Amber simulation *on the PDB file of the protein*.

```
git clone http://gitlab.oit.duke.edu/zns/proteins-reassemble.git 'myprotein'
```

Create a PDB file of whatever you want to simulate. (OPTIONAL) In `1-disassemble`, for example
there is `disassemble.conf` which is the NAMD configuration file for melting a protein.

Once you have a protein, copy it into `2-simulate` folder.
Then run
```
python setup.py
```
and enter the name of your file.

## Issues

The PDB file that you want to simulate needs to change "CD  ILE" to "CD1 ILE".
Also needs to change "HSD" to "HIS".
Also need to remove the entire line that has "OT1" and "OT2".
